package main

import (
	"fmt"
	"io"
	"net/http"

	"gitlab.com/linalinn/goshorturl/apiv1"
	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"
	"gitlab.com/linalinn/goshorturl/shorturl"
)

func setup(DB db.DB) {
	//reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter Admin Username:")
	//username, _ := reader.ReadString('\n')
	fmt.Print("Enter Admin Email:")
	//email, _ := reader.ReadString('\n')
	fmt.Print("Enter Admin Password:")
	//password, _ := reader.ReadString('\n')
	//DB.CreateUser(username, password, email, "", true)

}

func error404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	response := "ERROR 404:" + r.URL.Path + " not found"
	io.WriteString(w, response)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	config.Debug("redirect", r)
	if r.URL.Path == "/" {
		io.WriteString(w, "Main Page")
		return
	}
	client := db.OpenDB(config.Config.DataBaseURL)
	defer client.CloseDB()
	w.Header().Set("Cache-Control", "no-cache")
	if shorturl.Request_URI_Exists(r.URL.Path[1:], &client) {

		redirect := shorturl.FindRedirectByRequest_URI(r.URL.Path[1:], &client)
		if redirect.PAGE {
			io.WriteString(w, redirect.PAGE_DATA)
			redirect.Click(&client)
			return
		}
		http.Redirect(w, r, redirect.URL, http.StatusTemporaryRedirect)
		redirect.Click(&client)
		return
	}
	error404(w, r)
}

func main() {
	fmt.Println("GoShortUrl Server Version 0.2")
	fmt.Println("------------------------------")
	config.InitConfig()

	client := db.OpenDB(config.Config.DataBaseURL)
	defer client.CloseDB()
	api := apiv1.NewAPI(&client)

	http.HandleFunc("/", redirect)
	http.HandleFunc("/favicon.ico", error404)
	http.HandleFunc("/api/go/v1/add", api.Add)
	http.HandleFunc("/api/go/v1/update", api.Update)
	http.HandleFunc("/api/go/v1/remove", api.Remove)
	http.HandleFunc("/api/go/v1/lookup", api.Lookup)
	http.HandleFunc("/api/go/v1/list", api.List)
	if config.Config.EnableRegistration {
		http.HandleFunc("/api/go/v1/register", api.Register)
	}
	http.HandleFunc("/api/go/v1/get_new_api_key", api.Get_new_api_key)
	http.HandleFunc("/api/go/v1/login", api.Login)
	http.HandleFunc("/api/go/v1/logout", api.Logout)
	http.HandleFunc("/api/go/v1/user_remove", api.UserRemove)
	http.HandleFunc("/api/go/v1", api.Index)

	if config.Config.ServerTLS {
		config.Debug("http:", "Serving TLS at "+config.Config.ServerTLSAddr)
		go http.ListenAndServeTLS(config.Config.ServerTLSAddr, config.Config.ServerTLSCertFile, config.Config.ServerTLSKeyFile, nil)
	}
	if !config.Config.DisablePlain {
		config.Debug("http:", "Serving Plain Text at "+config.Config.ServerAddr)
		http.ListenAndServe(config.Config.ServerAddr, nil)
	}

}

/* DUMP

func readPost(len_string string) string {
	reader := bufio.NewReader(os.Stdin)
	len_data, _ := strconv.ParseInt(len_string, 10, 64)
	raw_data := make([]byte, len_data)
	reader.Read(raw_data)
	return string(raw_data)

}

u, e := url.Parse(os.Getenv("REQUEST_URI"))
		if e != nil {
			fmt.Println("Content-Type: text/plain;charset=UTF-8\n\n")
			fmt.Println(e)
			os.Exit(1)
		}

*/
