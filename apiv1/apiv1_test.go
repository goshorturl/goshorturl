package apiv1

import (
	"reflect"
	"testing"

	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"
	"gitlab.com/linalinn/goshorturl/user"
)

func TestAPIv1_CreateApiKey(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type fields struct {
		database *db.DB
	}
	type args struct {
		userID  string
		AppName string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   Apikey
	}{
		{"addApikey", fields{&client}, args{user.FindUserByEmail("lina@exsample.com", &client).ID, "app"}, Apikey{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			api := &APIv1{
				database: tt.fields.database,
			}
			if got := api.CreateApiKey(tt.args.userID, tt.args.AppName); reflect.DeepEqual(got, tt.want) {
				t.Errorf("APIv1.CreateApiKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAPIv1_GetUserIDbyApiKey(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type fields struct {
		database *db.DB
	}
	type args struct {
		ApiKey string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{"userif", fields{&client}, args{"8f6b4836ce98d1356af8e2be10ac16fa3f15815956f2b786ffd16f74ff1720ca"}, "bfaab0a230c83a0d3bebe1ea68ec733149e65f161a0828f885d0314df3972051"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			api := &APIv1{
				database: tt.fields.database,
			}
			if got := api.GetUserIDbyApiKey(tt.args.ApiKey); got != tt.want {
				t.Errorf("APIv1.GetUserIDbyApiKey() = %v, want %v", got, tt.want)
			}
		})
	}
}
