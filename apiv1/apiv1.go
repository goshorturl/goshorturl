package apiv1

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"
	"gitlab.com/linalinn/goshorturl/shorturl"
	"gitlab.com/linalinn/goshorturl/user"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Apikey struct {
	ObjID   primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	USER_ID string             `bson:"USER_ID"`
	KEY     string             `bson:"KEY"`
}

type APIv1 struct {
	database *db.DB
}

func (api *APIv1) Index(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	io.WriteString(w, "INDEX Hello world!"+r.URL.Path+r.URL.Hostname())
}

func (api *APIv1) Add(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	config.Debug("api Add", r)
	if r.Form["key"] != nil {
		if api.CheckApikey(r.Form["key"][0]) {
			if r.Form["url"] != nil {
				if r.Form["shorturl"] != nil {
					if shorturl.Request_URI_Exists(r.Form["shorturl"][0], api.database) {
						w.WriteHeader(http.StatusBadRequest)
						io.WriteString(w, r.Form["shorturl"][0]+" taken")
						return
					} else {
						newShorturl := shorturl.CreateRedirectCustume(r.Form["url"][0], r.Form["shorturl"][0], api.GetUserIDbyApiKey(r.Form["key"][0]), api.database)
						w.WriteHeader(http.StatusOK)
						io.WriteString(w, config.Config.ServerURL+"/"+newShorturl.REQUEST_URI)
						return
					}
				} else {
					newShorturl := shorturl.CreateRedirect(r.Form["url"][0], api.GetUserIDbyApiKey(r.Form["key"][0]), api.database)
					w.WriteHeader(http.StatusOK)
					io.WriteString(w, config.Config.ServerURL+"/"+newShorturl.REQUEST_URI)
					return
				}
			} else {
				w.WriteHeader(http.StatusBadRequest)
				io.WriteString(w, "Bad Request")
				return
			}

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid ApiKey")
			config.Debug("api Add", "Invalid ApiKey", r.Form["key"][0])
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, "Bad Request")
	return

}

func (api *APIv1) Update(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["key"] != nil {
		if api.CheckApikey(r.Form["key"][0]) {
			if r.Form["newurl"] != nil {
				if r.Form["shorturl"] != nil {
					if shorturl.Request_URI_Exists(r.Form["shorturl"][0], api.database) {
						redirect := shorturl.FindRedirectByRequest_URI(r.Form["shorturl"][0], api.database)
						redirect.URL = r.Form["newurl"][0]
						if api.GetUserIDbyApiKey(r.Form["key"][0]) != redirect.USER_ID {
							w.WriteHeader(http.StatusUnauthorized)
							return

						}
						if !redirect.Update(api.database) {
							w.WriteHeader(http.StatusInternalServerError)
							io.WriteString(w, "Internal Server error")
							return
						}
						w.WriteHeader(http.StatusOK)
						io.WriteString(w, "Ok")
						return
					} else {
						w.WriteHeader(http.StatusNotFound)
						io.WriteString(w, "404")
						return
					}
				} else {
					w.WriteHeader(http.StatusBadRequest)
					io.WriteString(w, "Bad Request")
					return
				}
			} else {
				w.WriteHeader(http.StatusBadRequest)
				io.WriteString(w, "Bad Request")
				return
			}

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid ApiKey")
			return
		}
	}
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, "Bad Request")
	return

}

func (api *APIv1) Remove(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if api.CheckApikey(r.Form["key"][0]) {
		if shorturl.Request_URI_Exists(r.Form["shorturl"][0], api.database) {
			redirect := shorturl.FindRedirectByRequest_URI(r.Form["shorturl"][0], api.database)
			u := user.FindUserByUserID(api.GetUserIDbyApiKey(r.Form["key"][0]), api.database)
			if u.ID == redirect.USER_ID || u.ADMIN {
				redirect.Delete(api.database)
				w.WriteHeader(http.StatusOK)
				io.WriteString(w, "Ok")
				return
			} else {
				w.WriteHeader(http.StatusUnauthorized)
				io.WriteString(w, "Unauthorized")
				return
			}
		}
	}
}

func (api *APIv1) Lookup(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["key"] != nil {
		if api.CheckApikey(r.Form["key"][0]) {
			if shorturl.Request_URI_Exists(r.Form["shorturl"][0], api.database) {
				w.WriteHeader(http.StatusOK)
				io.WriteString(w, r.Form["shorturl"][0]+" taken")
				return
			} else {
				w.WriteHeader(http.StatusNotFound)
				io.WriteString(w, r.Form["shorturl"][0]+" free")
				return
			}

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid ApiKey")
			return
		}
	}
	w.WriteHeader(http.StatusInternalServerError)
	io.WriteString(w, "Internal Server error")
	return

}

func (api *APIv1) List(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["key"] != nil {
		if api.CheckApikey(r.Form["key"][0]) {
			jsonlist, err := json.Marshal(shorturl.FindRedirectsByUserId(api.GetUserIDbyApiKey(r.Form["key"][0]), api.database))
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Internal Server error")
				return

			}
			io.WriteString(w, string(jsonlist))
			return

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid ApiKey")
			return
		}
	}
	w.WriteHeader(http.StatusInternalServerError)
	io.WriteString(w, "Internal Server error")
	return

}

func (api *APIv1) Register(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["username"] != nil && r.Form["email"] != nil && r.Form["passwd"] != nil {
		if user.UserExistsByEmail(r.Form["email"][0], api.database) || user.UserExistsByName(r.Form["username"][0], api.database) {
			w.WriteHeader(http.StatusBadRequest)
			io.WriteString(w, "email or name in use")
			return

		} else {
			newUser, err := user.CreateUser(r.Form["username"][0], r.Form["passwd"][0], r.Form["email"][0], "", false, api.database)
			if err != nil {
				log.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Internal Server error")
				return
			}
			config.Debug("api Refister", newUser)
			w.WriteHeader(http.StatusOK)
			io.WriteString(w, "Ok")
			return

		}
	}
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, "Bad Request")
	return

}

func (api *APIv1) Get_new_api_key(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["username"] != nil && r.Form["passwd"] != nil && r.Form["application"] != nil {
		if user.UserExistsByName(r.Form["username"][0], api.database) {
			user := user.FindUserByName(r.Form["username"][0], api.database)
			if user.CheckPassword(r.Form["passwd"][0]) {
				newAPIKey := api.CreateApiKey(user.ID, r.Form["application"][0])
				w.WriteHeader(http.StatusOK)
				io.WriteString(w, newAPIKey.KEY)
				return

			} else {
				w.WriteHeader(http.StatusUnauthorized)
				io.WriteString(w, "Invalid Name or Password")
				return
			}

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid Name or Password")
			return

		}
	}
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, "Bad Request")
	return

}

func (api *APIv1) UserRemove(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["key"] != nil {
		if api.CheckApikey(r.Form["key"][0]) {
			User := user.FindUserByUserID(api.GetUserIDbyApiKey(r.Form["key"][0]), api.database)
			if (r.Form["username"]) != nil && User.ADMIN {
				user.FindUserByName(r.Form["username"][0], api.database)
			} else if (r.Form["username"]) != nil && !User.ADMIN {
				w.WriteHeader(http.StatusUnauthorized)
			}
			User.Delete(api.database)
		}
	}

}

func (api *APIv1) Login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	if r.Form["username"] != nil && r.Form["passwd"] != nil {
		if user.UserExistsByName(r.Form["username"][0], api.database) {
			user := user.FindUserByName(r.Form["username"][0], api.database)
			if user.CheckPassword(r.Form["passwd"][0]) {
				http.SetCookie(w, db.NewSession(user.ID, api.database))
				w.WriteHeader(http.StatusOK)
				io.WriteString(w, "Ok")
				return

			} else {
				w.WriteHeader(http.StatusUnauthorized)
				io.WriteString(w, "Invalid Name or Password")
				return
			}

		} else {
			w.WriteHeader(http.StatusUnauthorized)
			io.WriteString(w, "Invalid Name or Password")
			return

		}
	}
	w.WriteHeader(http.StatusBadRequest)
	io.WriteString(w, "Bad Request")
	return

}

func (api *APIv1) Logout(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("session")
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		io.WriteString(w, "Bad Request")
		return
	}
	db.DeleteSession(cookie.Value, api.database)
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, "Ok")

}

func NewAPI(DB *db.DB) APIv1 {
	return APIv1{database: DB}
}

func (api *APIv1) CreateApiKey(userID, AppName string) Apikey {
	newApiKey := Apikey{primitive.NewObjectID(), userID, db.Hash(userID + time.Now().String() + AppName)}
	api.database.InsertOne("apikeys", newApiKey)
	return newApiKey

}

func (api *APIv1) CheckApikey(ApiKey string) bool {
	if ApiKey == "" {
		return false
	}
	if ApiKey == FindAPIKeyOne(bson.D{{"KEY", ApiKey}}, api.database).KEY {
		return true
	}
	return false
}

func (api *APIv1) GetUserIDbyApiKey(ApiKey string) string {
	return FindAPIKeyOne(bson.D{{"KEY", ApiKey}}, api.database).USER_ID
}

func FindAPIKeyOne(query bson.D, database *db.DB) Apikey {
	config.Debug("FindAPIKey", query)
	data := (database.FindOne(query, "apikeys")).(primitive.D).Map()
	if len(data) == 0 {
		config.Debug("FindAPIKey", "No Key found")
		return Apikey{}
	}
	config.Debug("FindAPIKey", "Found:\n", data)
	return apiKeyFromMap(data)
}

func apiKeyFromMap(datamap map[string]interface{}) Apikey {
	return Apikey{datamap["_id"].(primitive.ObjectID), datamap["USER_ID"].(string), datamap["KEY"].(string)}
}
