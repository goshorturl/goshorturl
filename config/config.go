package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
)

type Configuration struct {
	DataBaseURL        string   `json:"DataBaseURL"`
	DataBaseUser       string   `json:"DataBaseUser"`
	DataBasePW         string   `json:"DataBasePW"`
	DataBaseName       string   `json:"DataBaseName"`
	ServerAddr         string   `json:"ServerAddr"`
	ServerURL          string   `json:"ServerURL"`
	DisablePlain       bool     `json:"DisablePlain"`
	ServerTLS          bool     `json:"ServerTLS"`
	ServerTLSAddr      string   `json:"ServerTLSAddr"`
	ServerTLSCertFile  string   `json:"ServerTLSCertFile"`
	ServerTLSKeyFile   string   `json:"ServerTLSKeyFile"`
	EnableRegistration bool     `json:"EnableRegistration"`
	ShorturlLength     int      `json:"ShorturlLength"`
	ShorturlLetters    string   `json:"ShorturlLetters"`
	ReservedPrefix     []string `json:"ReservedPrefix"`
}

var (
	Config                  *Configuration
	ReservedPrefixByProgram *[]string
	Debuging                bool
)

func InitConfig() {
	Config = &Configuration{
		DataBaseURL:        "mongodb://localhost:27017",
		DataBaseName:       "goshorturl",
		ServerAddr:         ":8001",
		ServerURL:          "loaclhost",
		DisablePlain:       false,
		ServerTLS:          false,
		ServerTLSAddr:      ":4433",
		ServerTLSCertFile:  "",
		ServerTLSKeyFile:   "",
		EnableRegistration: true,
		ShorturlLength:     7,
		ShorturlLetters:    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
		ReservedPrefix:     []string{"/api"},
	}

	path := flag.String("c", "/etc/goshorturl.conf", "Config File")
	flag.BoolVar(&Debuging, "debug", Debuging, "enables Debug msg")
	flag.Parse()
	confFile := loadConfigFile(*path)
	Config = &confFile
	ReservedPrefixByProgram = &[]string{"/api"}
	/*
		*Config = loadConfigFile("/etc/goshorturl.conf")
		DataBaseURLCMD := flag.String("db-url", Config.DataBaseURL, "URL of Mongo DB default mongodb://localhost:27017")
		DataBaseUserCMD := flag.String("db-user", Config.DataBaseUser, "User for Mongo DB default none")
		DataBasePWCMD := flag.String("db-pw", Config.DataBasePW, "Password for Mongo DB default none")
		DataBaseNameCMD := flag.String("db-name", Config.DataBaseName, "Name of Database in Mongo DB default goshorturl")
		ServerAddrCMD := flag.String("server-addr", Config.ServerAddr, "Addr of goshorturl server default :8001")
		DisablePlainCMD := flag.Bool("disable-plain", Config.DisablePlain, "disable redirect to TLS Server")
		ServerTLSCMD := flag.Bool("tls", Config.ServerTLS, "enables TLS")
		ServerTLSAddrCMD := flag.String("tls-addr", Config.ServerTLSAddr, "TLS Addr of goshorturl server default :4433")
		ServerTLSCertFileCMD := flag.String("tls-cert", Config.ServerTLSCertFile, "CertFile default none")
		ServerTLSKeyFileCMD := flag.String("tls-key", Config.ServerTLSKeyFile, "KeyFile default none")
		flag.BoolVar(&Debuging, "debug", Debuging, "enables Debug msg")
		path := flag.String("c", "/etc/goshorturl.conf", "Config File")
		flag.Parse()
		confFile := loadConfigFile(*path)

		Config.DataBaseURL = confFile.DataBaseURL
		Config.DataBaseUser = confFile.DataBaseUser
		Config.DataBasePW = confFile.DataBasePW
		Config.DataBaseName = confFile.DataBaseName
		Config.ServerAddr = confFile.ServerAddr
		Config.DisablePlain = confFile.DisablePlain
		Config.ServerTLS = confFile.ServerTLS
		Config.ServerTLSAddr = confFile.ServerTLSAddr
		Config.ServerTLSCertFile = confFile.ServerTLSCertFile
		Config.ServerTLSKeyFile = confFile.ServerTLSKeyFile

		Config.DataBaseURL = DataBaseURLCMD
		Config.DataBaseUser = DataBaseUserCMD
		Config.DataBasePW = DataBasePWCMD
		Config.DataBaseName = DataBaseNameCMD
		Config.ServerAddr = ServerAddrCMD
		Config.DisablePlain = DisablePlainCMD
		Config.ServerTLS = ServerTLSCMD
		Config.ServerTLSAddr = ServerTLSAddrCMD
		Config.ServerTLSCertFile = ServerTLSCertFileCMD
		Config.ServerTLSKeyFile = ServerTLSKeyFileCMD
	*/
	Debug("DataBaseURL", Config.DataBaseURL)
	Debug("DataBaseUser", Config.DataBaseUser)
	Debug("DataBaseName", Config.DataBaseName)
	Debug("ServerAddr", Config.ServerAddr)
	Debug("ServerURL", Config.ServerURL)
	Debug("DisablePlain", Config.DisablePlain)
	Debug("ServerTLS", Config.ServerTLS)
	Debug("ServerTLSAddr", Config.ServerTLSAddr)
	Debug("ServerTLSCertFile", Config.ServerTLSCertFile)
	Debug("ServerTLSKeyFile", Config.ServerTLSKeyFile)
	Debug("EnableRegistration", Config.EnableRegistration)
	Debug("ShorturlLength", Config.ShorturlLength)
	Debug("ShorturlLetters", Config.ShorturlLetters)

}

func Debug(tag string, a ...interface{}) {
	if Debuging {
		fmt.Print(tag + ":")
		fmt.Println(a...)
	}

}

func loadConfigFile(file_path string) Configuration {
	Debug("loadConfig", "File:"+file_path)
	data_raw, err := ioutil.ReadFile(file_path)
	if err != nil {
		log.Panic("Fail: Reading file " + file_path + "\n")
	}
	Debug("Conf File", string(data_raw))
	data := Configuration{}
	err = json.Unmarshal(data_raw, &data)
	if err != nil {
		log.Panic("Json Load error", err)
	}
	return data

}
