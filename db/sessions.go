package db

import (
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Session struct {
	ObjID   primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	USER_ID string             `bson:"USERID"`
	KEY     string             `bson:"KEY"`
	EXPIRES int64              `bson:"EXPIRES"`
}

func NewSession(userID string, database *DB) *http.Cookie {
	database.DeleteAll("sessions", bson.D{{"EXPIRES", bson.D{{"$lt", time.Now().Unix()}}}})
	Expires := time.Now().Add(300 * time.Second)
	NewSession := Session{primitive.NewObjectID(), userID, Hash(strconv.FormatInt(rand.New(rand.NewSource(time.Now().UnixNano())).Int63(), 10)), Expires.Unix()}
	database.InsertOne("sessions", NewSession)
	return &http.Cookie{
		Name:    "session",
		Value:   NewSession.KEY,
		Expires: Expires,
	}
}

func GetSession(key string, database *DB) Session {
	data := (database.FindOne(bson.D{{"KEY", key}}, "sessions")).(primitive.D).Map()
	if len(data) == 0 {
		return Session{}
	}
	return sessionFromMap(data)

}

func DeleteSession(key string, database *DB) {
	database.DeleteOne("sessions", GetSession(key, database).ObjID)

}

func sessionFromMap(datamap map[string]interface{}) Session {
	return Session{datamap["_id"].(primitive.ObjectID), datamap["USERID"].(string), datamap["KEY"].(string), datamap["EXPIRES"].(int64)}
}
