package db

import (
	"net/http"
	"reflect"
	"testing"

	"gitlab.com/linalinn/goshorturl/config"
)

func TestNewSession(t *testing.T) {
	config.InitConfig()
	client := OpenDB("mongodb://localhost:27017")
	type args struct {
		userID   string
		database *DB
	}
	tests := []struct {
		name string
		args args
		want http.Cookie
	}{
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
		{"Session", args{"userid", &client}, http.Cookie{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewSession(tt.args.userID, tt.args.database); reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewSession() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetSession(t *testing.T) {
	config.InitConfig()
	client := OpenDB("mongodb://localhost:27017")
	type args struct {
		key      string
		database *DB
	}
	tests := []struct {
		name string
		args args
		want Session
	}{
		{"GetSession", args{"", &client}, Session{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetSession(tt.args.key, tt.args.database); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetSession() = %v, want %v", got, tt.want)
			}
		})
	}
}
