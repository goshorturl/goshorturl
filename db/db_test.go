package db

import (
	"testing"
)

func TestHash(t *testing.T) {
	type args struct {
		data string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"hash1", args{"abcd"}, "88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589"},
		{"hash2", args{"l38jdl,30sldmsm3okdfj30"}, "b237c29ab2049724f2e16f4d9094b76b7ea36fe41dfc55eb4560af027f82a311"},
		{"hash3", args{"ü😐 😑 😶 🙄 😏 😣 ö\"\"👪 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍"}, "f80f8acdbbbf218d02b0a1fcbcbb23bbcc23a53cd6c0193971a3777857e4622b"},
		{"hash4", args{"a😐 😑 😶 🙄 😏 😣 b\"cd\""}, "e3d6c62f3a3925e498198544f583e51ed319bf9a438300e0e106952beb7509c4"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Hash(tt.args.data); got != tt.want {
				t.Errorf("Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}
