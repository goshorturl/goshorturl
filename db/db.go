package db

import (
	"context"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"log"
	"os"
	"time"

	"gitlab.com/linalinn/goshorturl/config"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	ObjID   primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	NAME    string             `bson:"NAME"`
	PASSWD  string             `bson:"PASSWD"`
	ID      string             `bson:"ID"`
	EMAIL   string             `bson:"EMAIL"`
	YubiKey string             `bson:"YubiKey"`
	ADMIN   bool               `bson:"ADMIN"`
}

type DB struct {
	client  mongo.Client
	context *context.Context
}

func OpenDB(url string) DB {
	config.Debug("DB", "Conection to "+config.Config.DataBaseURL)
	ctx := context.Background() //context.WithTimeout(context.Background(), 60*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(url))
	if err != nil {
		os.Exit(1)
	}
	db := DB{client: *client, context: &ctx}
	db.DeleteAll("sessions", bson.D{{"EXPIRES", bson.D{{"$lt", time.Now().Unix()}}}})
	return db
}

func (db *DB) CloseDB() {
	db.client.Disconnect(*db.context)
}

func (db *DB) FindOne(query bson.D, colletionname string) interface{} {
	r := primitive.D{}
	db.client.Database(config.Config.DataBaseName).Collection(colletionname).FindOne(*db.context, query).Decode(&r)
	return r
}

func (db *DB) FindAll(query bson.D, collectionname string) []primitive.D {
	r := []primitive.D{}
	cursor, err := db.client.Database(config.Config.DataBaseName).Collection(collectionname).Find(*db.context, query)
	if err != nil {
		log.Println("Cursor error")
		log.Println(err)
		err = nil
	}
	err = cursor.All(*db.context, &r)
	if err != nil {
		log.Println("All")
		log.Println(err)
	}
	return r
}

func (db *DB) InsertOne(collention string, document interface{}) bool {
	_, err := db.client.Database(config.Config.DataBaseName).Collection(collention).InsertOne(*db.context, document)
	if err != nil {
		log.Fatalln(err)
		return false
	}
	return true

}

func (db *DB) UpdateOne(collection string, document interface{}, ObjID primitive.ObjectID) bool {
	db.client.Database(config.Config.DataBaseName).Collection(collection).ReplaceOne(*db.context, bson.D{{"_id", ObjID}}, toDoc(document))
	return true
}

func (db *DB) DeleteOne(collection string, ObjID primitive.ObjectID) {
	db.client.Database(config.Config.DataBaseName).Collection(collection).DeleteOne(*db.context, bson.D{{"_id", ObjID}})
}

func (db *DB) DeleteAll(collection string, document interface{}) {
	db.client.Database(config.Config.DataBaseName).Collection(collection).DeleteMany(*db.context, document)
}

func toDoc(v interface{}) bson.D {
	data, err := bson.Marshal(v)
	if err != nil {
		return bson.D{}
	}
	doc := bson.D{}
	err = bson.Unmarshal(data, &doc)
	return doc
}

func Hash(data string) string {
	hasher := sha1.New()
	hasher.Write([]byte(data))
	h := sha256.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
	//hex.EncodeToString(sha256.Sum256([]byte(data)))
	//hash := crypto.SHA256.New()
	//return b64.EncodeToString(hash.Sum256([]byte(data)))
	//return string(hash.Sum(nil))
}
