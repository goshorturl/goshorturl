package user

import (
	"log"
	"time"

	"gitlab.com/linalinn/goshorturl/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ObjID   primitive.ObjectID `bson:"_id,omitempty"`
	NAME    string             `bson:"NAME"`
	PASSWD  string             `bson:"PASSWD"`
	ID      string             `bson:"ID"`
	EMAIL   string             `bson:"EMAIL"`
	YubiKey string             `bson:"YubiKey"`
	ADMIN   bool               `bson:"ADMIN"`
}

func UserExistsByName(username string, database *db.DB) bool {
	UserTest := FindUserOne(bson.D{{"NAME", username}}, database)
	if UserTest.ObjID == primitive.NilObjectID {
		return false
	}
	return true

}

func UserExistsByEmail(email string, database *db.DB) bool {
	UserTest := FindUserOne(bson.D{{"EMAIL", email}}, database)
	if UserTest.ObjID == primitive.NilObjectID {
		return false
	}
	return true
}

func FindUserOne(query bson.D, database *db.DB) User {
	data := (database.FindOne(query, "users")).(primitive.D).Map()
	if len(data) == 0 {
		return User{}
	}
	return userFromMap(data)
}

func FindUserByEmail(email string, database *db.DB) User {
	return FindUserOne(bson.D{{"EMAIL", email}}, database)
}

func FindUserByName(name string, database *db.DB) User {
	return FindUserOne(bson.D{{"NAME", name}}, database)
}

func FindUserByUserID(UserID string, database *db.DB) User {
	return FindUserOne(bson.D{{"ID", UserID}}, database)
}

func CreateUser(username, passwd, EMAIL, Yubikey string, Admin bool, database *db.DB) (newUser User, err error) {
	id := generateID(EMAIL, username)
	newUser = User{primitive.NewObjectID(), username, createPasswd(id, passwd), id, EMAIL, Yubikey, Admin}

	database.InsertOne("users", newUser)
	if err != nil {
		log.Println(err)
		return User{}, err
	}
	return newUser, nil

}

func userFromMap(datamap map[string]interface{}) User {
	return User{datamap["_id"].(primitive.ObjectID), datamap["NAME"].(string), datamap["PASSWD"].(string), datamap["ID"].(string), datamap["EMAIL"].(string), datamap["YubiKey"].(string), datamap["ADMIN"].(bool)}
}

func generateID(email, username string) string {
	return db.Hash(email + username + time.Now().String())
}

func createPasswd(id, passwd string) string {
	return db.Hash(id + passwd)

}

func (user *User) CheckPassword(passwd string) bool {
	if passwd == "" {
		return false
	}
	if user.PASSWD == db.Hash(user.ID+passwd) {
		if user.PASSWD != db.Hash(user.ID+passwd) {
			return false
		} else {
			return true
		}
	}
	return false
}

func (user *User) Update(database *db.DB) bool {
	return database.UpdateOne("users", user, user.ObjID)
}

func (user *User) Delete(database *db.DB) {
	database.DeleteOne("users", user.ObjID)
}
