package user

import (
	"fmt"
	"testing"

	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"
)

func TestUserExistsByName(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type args struct {
		username string
		database *db.DB
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"lina", args{"lina", &client}, true},
		{"lina.cloud", args{"lina.cloud", &client}, true},
		{"No", args{"thisusernameistotalynotinthedatabasehoffeichjedenfalls", &client}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UserExistsByName(tt.args.username, tt.args.database); got != tt.want {
				t.Errorf("UserExistsByName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCreateUser(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type args struct {
		username string
		passwd   string
		EMAIL    string
		Yubikey  string
		Admin    bool
		database *db.DB
	}
	tests := []struct {
		name        string
		args        args
		wantNewUser User
		wantErr     bool
	}{
		{"Create User Test", args{"lina.cloud", "passwd", "lina@exsample.com", "", true, &client}, User{}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := CreateUser(tt.args.username, tt.args.passwd, tt.args.EMAIL, tt.args.Yubikey, tt.args.Admin, tt.args.database)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestUserExistsByEmail(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type args struct {
		email    string
		database *db.DB
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"lina", args{"lina@emailnotexists.invalideTLD", &client}, false},
		{"lina.cloud", args{"lina@exsample.com", &client}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := UserExistsByEmail(tt.args.email, tt.args.database); got != tt.want {
				t.Errorf("UserExistsByEmail() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_CheckPassword(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	FindUserByEmail("lina@exsample.com", &client)
	type args struct {
		passwd string
	}
	tests := []struct {
		name string
		user User
		args args
		want bool
	}{
		{"wrong", FindUserByEmail("lina@exsample.com", &client), args{"passwd1"}, false},
		{"wrong_utf8", FindUserByEmail("lina@exsample.com", &client), args{"ü😐 😑 😶 🙄 😏 😣 ö\"\"👪 👨‍👩‍👧 👨‍👩‍👧‍👦 👨‍👩‍👦‍👦 👨‍👩‍👧‍👧 👩‍👩‍"}, false},
		{"okay", FindUserByEmail("lina@exsample.com", &client), args{"passwd"}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.user.CheckPassword(tt.args.passwd); got != tt.want {
				t.Errorf("User.CheckPassword() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUser_Updata_by_Hand(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	usermodify := FindUserByEmail("lina@exsample.com", &client)
	t.Log("usermodify")
	t.Log(usermodify)
	t.Log()
	usermodify.NAME = "new Mod"
	usermodify.Update(&client)
	usermodify = FindUserByEmail("lina@exsample.com", &client)
	t.Log("usermodify")
	t.Log(usermodify)
	t.Log()
	if usermodify.NAME != "new Mod" {
		t.Fatal("Update Fail")
	}
	usermodify.NAME = "lina.cloud"
	usermodify.Update(&client)

}

func TestUser_Update(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	//userUnmod := FindUserByEmail("lina@exsample.com", client)
	usermodify := FindUserByEmail("lina@exsample.com", &client)
	userUnmod := usermodify
	usermodify.NAME = "new Mod"
	type args struct {
		database *db.DB
	}
	tests := []struct {
		name string
		user User
		args args
		want bool
	}{
		{"update user", usermodify, args{&client}, true},
		//{"update user", User{}, args{client}, false},
		{"update user", userUnmod, args{&client}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.user.Update(tt.args.database); got != tt.want {
				t.Errorf("User.Update() = %v, want %v", got, tt.want)
			}
			fmt.Println(FindUserByEmail("lina@exsample.com", &client))
		})
	}
}
