package shorturl

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func TestCreateRedirect(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type args struct {
		url      string
		USER_ID  string
		database *db.DB
	}
	tests := []struct {
		name string
		args args
		want Redirect
	}{
		{"create new", args{"https://exsample.com/", "645ff5dedfd6db7f9c4a16d4f5b9b09090565161e819270260780e22a6293684", &client}, Redirect{primitive.NewObjectID(), "asd", false, "", "url", "USER_ID", time.Now().Unix(), 0}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CreateRedirect(tt.args.url, tt.args.USER_ID, tt.args.database); reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateRedirect() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_genShorturl(t *testing.T) {
	config.InitConfig()
	client := db.OpenDB("mongodb://localhost:27017")
	type args struct {
		database *db.DB
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"test1", args{&client}, ""},
		{"test2", args{&client}, ""},
		{"test3", args{&client}, ""},
		{"test4", args{&client}, ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := genShorturl(tt.args.database); got != tt.want {
				t.Errorf("genShorturl() = %v, want %v", got, tt.want)
			}
		})
	}
}
