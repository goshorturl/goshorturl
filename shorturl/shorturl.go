package shorturl

import (
	"math/rand"
	"time"

	"gitlab.com/linalinn/goshorturl/config"
	"gitlab.com/linalinn/goshorturl/db"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/bson"
)

type Redirect struct {
	ObjID       primitive.ObjectID `json:"_id" bson:"_id,omitempty"`
	REQUEST_URI string             `json:"REQUESTURI" bson:"REQUESTURI"`
	PAGE        bool               `json:"PAGE" bson:"PAGE"`
	PAGE_DATA   string             `json:"REDIRECTPAGEDATA" bson:"REDIRECTPAGEDATA"`
	URL         string             `json:"URL" bson:"URL"`
	USER_ID     string             `json:"USERID" bson:"USERID"`
	CREATED_AT  int64              `json:"CREATEDAT" bson:"CREATEDAT"`
	CLICK_COUNT int32              `json:"CLICKCOUNT" bson:"CLICKCOUNT"`
}

func FindRedirectOne(query bson.D, database *db.DB) Redirect {
	data := (database.FindOne(query, "redirects")).(primitive.D).Map()
	if len(data) == 0 {
		return Redirect{}
	}
	return redirectFromMap(data)
}

func FindRedirectAll(query bson.D, database *db.DB) []Redirect {
	redirects_raw := (database.FindAll(query, "redirects"))
	if len(redirects_raw) == 0 {
		return []Redirect{}
	}
	redirects := []Redirect{}
	for _, data := range redirects_raw {
		redirects = append(redirects, redirectFromMap(data.Map()))
	}
	return redirects
}

func (redirect *Redirect) Click(database *db.DB) {
	redirect.CLICK_COUNT += 1
	redirect.Update(database)

}

func (redirect *Redirect) Update(database *db.DB) bool {
	return database.UpdateOne("redirects", redirect, redirect.ObjID)

}

func (redirect *Redirect) Delete(database *db.DB) {
	database.DeleteOne("redirects", redirect.ObjID)
}

func Request_URI_Exists(uri string, database *db.DB) bool {
	request := FindRedirectOne(bson.D{{"REQUESTURI", uri}}, database)
	if request.ObjID == primitive.NilObjectID {
		return false
	}
	return true

}

func FindRedirectByRequest_URI(uri string, database *db.DB) Redirect {
	return FindRedirectOne(bson.D{{"REQUESTURI", uri}}, database)

}

func FindRedirectsByUserId(USER_ID string, database *db.DB) []Redirect {
	return FindRedirectAll(bson.D{{"USERID", USER_ID}}, database)
}

func FindRedirectsByURL(url string, database *db.DB) []Redirect {
	return FindRedirectAll(bson.D{{"URL", url}}, database)
}

func CreateRedirect(url, USER_ID string, database *db.DB) Redirect {
	newRedirect := Redirect{primitive.NewObjectID(), genShorturl(database), false, "", url, USER_ID, time.Now().Unix(), 0}
	database.InsertOne("redirects", newRedirect)
	return newRedirect

}

func CreateRedirectCustume(url, url_short, USER_ID string, database *db.DB) Redirect {
	newRedirect := Redirect{primitive.NewObjectID(), url_short, false, "", url, USER_ID, time.Now().Unix(), 0}
	database.InsertOne("redirects", newRedirect)
	return newRedirect

}

func CreateRedirectCustumeActive(url, url_short, USER_ID, page_data string, database *db.DB) Redirect {
	newRedirect := Redirect{primitive.NewObjectID(), url_short, true, page_data, url, USER_ID, time.Now().Unix(), 0}
	database.InsertOne("redirects", newRedirect)
	return newRedirect

}

func CreateRedirectActive(url, USER_ID, page_data string, database *db.DB) Redirect {
	newRedirect := Redirect{primitive.NewObjectID(), genShorturl(database), true, page_data, url, USER_ID, time.Now().Unix(), 0}
	database.InsertOne("redirects", newRedirect)
	return newRedirect

}

func genShorturl(database *db.DB) string {
	gen := func() string {
		rand.Seed(time.Now().UnixNano())
		var letters = []rune(config.Config.ShorturlLetters)
		url := make([]rune, config.Config.ShorturlLength)
		for i := range url {
			url[i] = letters[rand.Intn(len(letters)-1)]
		}
		return string(url)
	}
	for {
		var newURI = gen()
		if !Request_URI_Exists(newURI, database) && !contains(config.Config.ReservedPrefix, newURI) && contains(*config.ReservedPrefixByProgram, newURI) {
			return newURI
		}

	}
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func redirectFromMap(datamap map[string]interface{}) Redirect {
	return Redirect{datamap["_id"].(primitive.ObjectID), datamap["REQUESTURI"].(string), datamap["PAGE"].(bool), datamap["REDIRECTPAGEDATA"].(string), datamap["URL"].(string), datamap["USERID"].(string), datamap["CREATEDAT"].(int64), datamap["CLICKCOUNT"].(int32)}
}
